//
//  Constants.swift
//  empresas
//
//  Created by Beatriz Teles Castro on 14/01/20.
//  Copyright © 2020 Beatriz Teles Castro. All rights reserved.
//

import Foundation

struct K {
    
    struct CryptKeys {
        static let keyData = "PBKDF2WithHmacSHA1"
        static let ivData = "sampleText"
    }
    
    struct HostServer {
        
        static let apiIoasys = "https://empresas.ioasys.com.br/api/v1"

        
    }

    struct APIParameterKey {
        static let email                            = "email"
        static let password                         = "password"


    }
    
    enum HTTPHeaderField: String {
        case authentication   = "Authorization"
        case contentType      = "Content-Type"
        case accessToken      = "access-token"
        case uid      = "uid"
        case client      = "client"
        case acceptType       = "Accept"
        case acceptEncoding   = "Accept-Encoding"
        case acceptCharSet    = "Accept-Charset"
    }
    
    enum ContentType: String {
        case json = "application/json"
    }

}

