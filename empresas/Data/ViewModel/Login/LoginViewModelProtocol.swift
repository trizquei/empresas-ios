//
//  LoginViewModelProtocol.swift
//  empresas
//
//  Created by Beatriz Teles Castro on 14/01/20.
//  Copyright © 2020 Beatriz Teles Castro. All rights reserved.
//

import Foundation
import UIKit

protocol LoginViewModelDelegate: class {
    
    func errorMessageDidChange(message: String)
    func signinSuccess(email: String, password: String)
    func startLoading()
    func stopLoading()
    
    
}
protocol LoginViewModelProtocol {
    
    var viewDelegate: LoginViewModelDelegate? {get set}
    
    func submit(email : String, password : String)
    
}
