//
//  LoginViewModel.swift
//  empresas
//
//  Created by Beatriz Teles Castro on 14/01/20.
//  Copyright © 2020 Beatriz Teles Castro. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import UIKit
import Alamofire


class LoginViewModel: LoginViewModelProtocol {
    
    

    private var disposeBag = DisposeBag()
    
    var viewDelegate: LoginViewModelDelegate?
    
    fileprivate(set) var errorMessage: String = "" {
        didSet {
            viewDelegate?.errorMessageDidChange(message: errorMessage)
        }
    }
    
    
    func submit(email: String, password: String) {
        viewDelegate?.startLoading()
        if let uuid = UIDevice.current.identifierForVendor?.uuidString {
            UserClient.signinActionRX(email: email, password: password).observeOn(MainScheduler.instance).subscribe(
                onNext: { response in
                    
                    print("\n\nresponse:\n\(response.success)")
                    self.viewDelegate?.stopLoading()
                    self.viewDelegate?.signinSuccess(email: email, password: password)
                    
//                    SessionHandler.setToken(data: response.enterprise)
                },
                onError: { error in
                    self.viewDelegate?.stopLoading()
                
                    if let errorObject = error as? ErrorLogin {
                        print("Status \(errorObject.success)")
                        self.errorMessage = "Dados Invalidos"
                        debugPrint("Ocorreu um error no DashboardViewModel -> fetchBalance \(error)")
                    }
            }).disposed(by: disposeBag)
        }
    }
    
}
