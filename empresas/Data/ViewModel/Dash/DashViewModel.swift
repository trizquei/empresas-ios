//
//  DashViewModel.swift
//  empresas
//
//  Created by Beatriz Teles Castro on 15/01/20.
//  Copyright © 2020 Beatriz Teles Castro. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import UIKit
import Alamofire


class DashViewModel: DashViewModelProtocol {
    
    private var disposeBag = DisposeBag()
    
    var viewDelegate: DashViewModelDelegate?
    
    fileprivate(set) var errorMessage: String = "" {
        didSet {
            viewDelegate?.errorMessageDidChange(message: errorMessage)
        }
    }
    
    func feedingList() {
        viewDelegate?.startLoading()
        if let uuid =  UIDevice.current.identifierForVendor?.uuidString {
            EnterprisesClient.accessToAllRX().observeOn(MainScheduler.instance).subscribe(
            
                onNext: {
                    response in
                    self.viewDelegate?.stopLoading()
                    self.viewDelegate?.fillingListCompanies(lista: response.enterprises)
                },
                onError: { error in
                    self.viewDelegate?.stopLoading()
                    
                    if let errorObject = error as? ErrorLogin {
                        print("Status \(errorObject.success)")
                        self.errorMessage = "Dados Invalidos"
                        debugPrint("Ocorreu um error no DashboardViewModel -> fetchBalance \(error)")
                        
                    }
            }).disposed(by: disposeBag)
        }
    }
}
