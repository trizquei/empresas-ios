//
//  DashViewModelProtocol.swift
//  empresas
//
//  Created by Beatriz Teles Castro on 15/01/20.
//  Copyright © 2020 Beatriz Teles Castro. All rights reserved.
//

import Foundation
import UIKit

protocol DashViewModelDelegate: class {
    
    func errorMessageDidChange(message: String)
    func fillingListCompanies(lista: [Enterpris])
    func startLoading()
    func stopLoading()
    
    
}
protocol DashViewModelProtocol {
    
    var viewDelegate: DashViewModelDelegate? {get set}
    
    func feedingList()
    
}
