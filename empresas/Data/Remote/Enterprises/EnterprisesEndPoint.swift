//
//  EnterprisesEndPoint.swift
//  empresas
//
//  Created by Beatriz Teles Castro on 14/01/20.
//  Copyright © 2020 Beatriz Teles Castro. All rights reserved.
//

import Foundation
import Alamofire

enum EnterprisesEndPoint: APIConfiguration {
    
    case enterprisesAllPropertys(type_enterprises: String, name_enterprises: String)
    case enterprisesList
    
    // HTTPMethod
    var method: HTTPMethod {
        switch self {
        case .enterprisesAllPropertys:
            return .get
            
        case .enterprisesList:
            return .get
        }
    }
    
    
    // Path
    var path: String {
        switch self {
        case .enterprisesAllPropertys(let type_enterprises, let name_enterprises):
            return "enterprises?enterprises_types=\(type_enterprises)&name=\(name_enterprises)"
        case .enterprisesList:
            return "/enterprises"
        }
    }
    
    // MARK: - URL query
    var baseURL: URL{
        switch self {
        case .enterprisesAllPropertys:
            return URL(string: "\(K.HostServer.apiIoasys)")!
        case .enterprisesList:
            return URL(string: "\(K.HostServer.apiIoasys)?")!
        }
    }
    
    // Header Parameters
    var headerParameters: [String : String]? {
        switch self {
        case .enterprisesAllPropertys:
            return [
                K.HTTPHeaderField.acceptType.rawValue : K.ContentType.json.rawValue,
                K.HTTPHeaderField.contentType.rawValue : K.ContentType.json.rawValue
            ]
            
        case .enterprisesList:
            return [
                K.HTTPHeaderField.acceptType.rawValue : K.ContentType.json.rawValue,
                K.HTTPHeaderField.contentType.rawValue : K.ContentType.json.rawValue
            ]
        }
    }
    
    // Body Parameters
    var parameters: Parameters? {
        switch self {
        case .enterprisesAllPropertys:
            return nil
        case .enterprisesList:
            return nil
        }
    }
    
    
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        
        let url = try baseURL.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
        // HTTP Method
        urlRequest.httpMethod = method.rawValue
        
        // Headers Parameter
        headerParameters?.forEach({ headerParameter in
            urlRequest.addValue(headerParameter.value, forHTTPHeaderField: headerParameter.key)
        })
        
        // Body Parameter
        if let parameters = parameters {
            do {
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
            } catch {
                throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
            }
        }
        
        return urlRequest
    }
}
