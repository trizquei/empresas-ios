//
//  EnterprisesClient.swift
//  empresas
//
//  Created by Beatriz Teles Castro on 15/01/20.
//  Copyright © 2020 Beatriz Teles Castro. All rights reserved.
//

import Alamofire
import RxSwift

class EnterprisesClient : BaseAPIConfiguration {

    static func accessToAllRX()-> Observable<ResultTeste> {
        return Observable.create({ (observable) -> Disposable in
            let request = performRequestAuthentication(route: EnterprisesEndPoint.enterprisesList) { (response) in
                
                if response?.response?.statusCode == 200 {
                    
                    if let data = response!.data, let utf8Text = String(data: data, encoding: .utf8) {
                        let json = utf8Text.data(using: .utf8)
                        
                        do {
                            
                            let decoder = JSONDecoder()
                            
                            let decoderJson = try decoder.decode(ResultTeste.self, from: json!)
                            
                            observable.onNext(decoderJson)
                            observable.onCompleted()
                        } catch let error {
                            observable.onError(error)
                            print("entrou no erro!! \(error.localizedDescription)")
                          
                        }
                        
                    }
                } else {
                    if let data = response!.data, let utf8Text = String(data: data, encoding: .utf8) {
                        let json = utf8Text.data(using: .utf8)
                        do {
                            let decoder = JSONDecoder()
                            let errorHttp = try decoder.decode(ErrorLogin.self, from: json!)
                            observable.onError(errorHttp)
                            
                        } catch let error {
                            print("Ocorreu um erro ao buscar os dados \(error)")
                        }
                    }
                    
                }
                observable.onError(ErrorLogin(success: false, errors: ["Nothing"]))
            }
            return Disposables.create {
                request.cancel()
            }
        })
    }
}
