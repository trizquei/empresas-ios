//
//  UserClient.swift
//  empresas
//
//  Created by Beatriz Teles Castro on 14/01/20.
//  Copyright © 2020 Beatriz Teles Castro. All rights reserved.
//

import Alamofire
import RxSwift

class UserClient : BaseAPIConfiguration {

    static func signinActionRX(email: String, password: String) -> Observable<ResultLogin> {
        return Observable.create({ (observable) -> Disposable in
            
            let request = performRequest(route: UserEndPoint.login(email: email, password: password)) { response in
                if response?.error == nil {
                    if let data = response!.data, let utf8Text = String(data: data, encoding: .utf8) {
                        let json = utf8Text.data(using: .utf8)
                        do {
                            let decoder = JSONDecoder()
                            let decoderJson = try decoder.decode(ResultLogin.self, from: json!)
                            
                            observable.onNext(decoderJson)
                            
                            observable.onCompleted()
                        } catch let error {
                            observable.onError(error)
                        }
                        let accessToken = response?.response?.httpHeaders["access-token"]
                        let uid = response?.response?.httpHeaders["uid"]
                        let client = response?.response?.httpHeaders["client"]
                        SessionHandler.setToken(data: accessToken ?? "")
                        SessionHandler.setUserId(data: uid ?? "")
                        SessionHandler.setClient(data: client ?? "")
                        print("\n\nO acess-token é \(accessToken ?? ""), uid é \(uid ?? "") e o client é \(client ?? "")\n\n\n")
                    }
                } else {
                    
                    if let data = response!.data, let utf8Text = String(data: data, encoding: .utf8) {
                        let json = utf8Text.data(using: .utf8)
                        do {
                            let decoder = JSONDecoder()
                            let errorHttp = try decoder.decode(ErrorLogin.self, from: json!)
                            print("ERROR \(errorHttp)")
                            observable.onError(errorHttp)
                            
                        } catch let error {
                            print("Ocorreu um erro ao buscar os dados \(error)")
                        }
                    }
                    
                    
                    
                }
            }
            
            return Disposables.create {
                request.cancel()
            }
        })
    }
    
}
