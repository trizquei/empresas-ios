//
//  UserEndPoint.swift
//  empresas
//
//  Created by Beatriz Teles Castro on 14/01/20.
//  Copyright © 2020 Beatriz Teles Castro. All rights reserved.
//

import Foundation
import Alamofire

enum UserEndPoint: APIConfiguration {
    case login(email: String, password: String)

    // HTTPMethod
    var method: HTTPMethod {
        switch self {
        case .login:
            return .post
        }
    }
    
    
    // Path
    var path: String {
        switch self {
        case .login:
            return "/users/auth/sign_in"
        }
    }
    
    // MARK: - URL query
    var baseURL: URL{
        switch self {
        case .login:
            return URL(string: "\(K.HostServer.apiIoasys)")!
        }
    }
    
    // Header Parameters
    var headerParameters: [String : String]? {
        switch self {
        case .login:
            return [
                K.HTTPHeaderField.acceptType.rawValue : K.ContentType.json.rawValue,
                K.HTTPHeaderField.contentType.rawValue : K.ContentType.json.rawValue,
                
            ]
        }
    }
    
    // Body Parameters
    var parameters: Parameters? {
        switch self {
        case .login(let email, let password):
            return [
                K.APIParameterKey.email:       email,
                K.APIParameterKey.password:       password,

            ]
        }
    }
    
    
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        
        let url = try baseURL.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
        // HTTP Method
        urlRequest.httpMethod = method.rawValue
        
        // Headers Parameter
        headerParameters?.forEach({ headerParameter in
            urlRequest.addValue(headerParameter.value, forHTTPHeaderField: headerParameter.key)
        })
        
        // Body Parameter
        if let parameters = parameters {
            do {
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
            } catch {
                throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
            }
        }
        
        return urlRequest
    }
}
