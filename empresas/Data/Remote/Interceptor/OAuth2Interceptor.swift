//
//  OAuth2Interceptor.swift
//  empresas
//
//  Created by Beatriz Teles Castro on 14/01/20.
//  Copyright © 2020 Beatriz Teles Castro. All rights reserved.
//

import Alamofire
import RxSwift

class OAuth2Interceptor: RequestInterceptor {
    
    private var disposeBag = DisposeBag()
    private var accessToken: String
    private var uid: String
    private var client: String
    
    init(accessToken : String, uid: String, client: String) {
        self.accessToken = accessToken
        self.uid = uid
        self.client = client
    }
    
//    func adapt(_ urlRequest: URLRequest, for session: Session, completion: @escaping (Result<URLRequest>) -> Void) {
//
//        var newUrlRequest = urlRequest
//
//        newUrlRequest.addValue(accessToken, forHTTPHeaderField: K.HTTPHeaderField.authentication.rawValue)
//
//        print("\n\n 🚀 Running request(OAuth): \n method: [ \(newUrlRequest.httpMethod ?? "") ] \n host: [ \(newUrlRequest.url?.absoluteString ?? "") ] \n header: [  \(newUrlRequest.httpHeaders.description ) ] \n body: [ \( newUrlRequest.httpBody == nil ? "nil" : OAuth2Interceptor.escapeAllSingleQuotes(String(data: newUrlRequest.httpBody!, encoding: String.Encoding.utf8)!) ) ]")
//
//        completion(.success(newUrlRequest))
//
//    }
    
    func adapt(_ urlRequest: URLRequest, for session: Session, completion: @escaping (Result<URLRequest>) -> Void) {

        var newUrlRequest = urlRequest

        newUrlRequest.addValue(accessToken, forHTTPHeaderField: K.HTTPHeaderField.accessToken.rawValue)
        newUrlRequest.addValue(uid, forHTTPHeaderField: K.HTTPHeaderField.uid.rawValue)
        newUrlRequest.addValue(client, forHTTPHeaderField: K.HTTPHeaderField.client.rawValue)

        print("\n\n 🚀 Running request(OAuth): \n method: [ \(newUrlRequest.httpMethod ?? "") ] \n host: [ \(newUrlRequest.url?.absoluteString ?? "") ] \n header: [  \(newUrlRequest.httpHeaders.description ) ] \n body: [ \( newUrlRequest.httpBody == nil ? "nil" : OAuth2Interceptor.escapeAllSingleQuotes(String(data: newUrlRequest.httpBody!, encoding: String.Encoding.utf8)!) ) ]")

        completion(.success(newUrlRequest))

    }
    
    /// Escapes all single quotes for shell from a given string.
    static func escapeAllSingleQuotes(_ value: String) -> String {
        return value.replacingOccurrences(of: "'", with: "'\\''")
    }

    
}

