//
//  LogInterceptor.swift
//  empresas
//
//  Created by Beatriz Teles Castro on 14/01/20.
//  Copyright © 2020 Beatriz Teles Castro. All rights reserved.
//

import Alamofire

class LogInterceptor: RequestInterceptor {
    
    init() { }
    
    func adapt(_ urlRequest: URLRequest, for session: Session, completion: @escaping (Result<URLRequest>) -> Void) {
        
        print("\n\n 🚀 Running request(Log): \n method: [ \(urlRequest.httpMethod ?? "") ] \n host: [ \(urlRequest.url?.absoluteString ?? "") ] \n header: [  \(urlRequest.httpHeaders.description ) ] \n body: [ \( urlRequest.httpBody == nil ? "nil" : OAuth2Interceptor.escapeAllSingleQuotes(String(data: urlRequest.httpBody!, encoding: String.Encoding.utf8)!) ) ] ")
        
        completion(.success(urlRequest))
        
    }
    
}
