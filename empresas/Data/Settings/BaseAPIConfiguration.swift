//
//  BaseAPIConfiguration.swift
//  empresas
//
//  Created by Beatriz Teles Castro on 14/01/20.
//  Copyright © 2020 Beatriz Teles Castro. All rights reserved.
//

import Alamofire
import Foundation

class BaseAPIConfiguration {
    
    // Adição de token na requisição
    @discardableResult
    static func performRequestAuthentication(route : APIConfiguration, completion : @escaping (DataResponse<Any>?)-> Void) -> DataRequest {
        if let token = SessionHandler.getToken() {
            return AF.request(route, interceptor: OAuth2Interceptor(accessToken: token, uid: SessionHandler.getUserId()!, client: SessionHandler.getClient()! )).validate().responseJSON { (response : DataResponse<Any>?) in
                print("\n🔮📝 Result Request: \(response?.description ?? "nil") \n")
                
                completion(response)
            }
        }
        return AF.request(route, interceptor: LogInterceptor()) .validate().responseJSON { (response : DataResponse<Any>?) in
            print("\n🔮⚠️ Result Request: \(response?.description ?? "nil") \n")
            completion(response)
        }
    }
    
    // Requisição sem token
    @discardableResult
    static func performRequest(route : APIConfiguration, completion : @escaping (DataResponse<Any>?)-> Void) -> DataRequest {
        return AF.request(route, interceptor: LogInterceptor()) .validate(statusCode: 200...207).responseJSON { (response : DataResponse<Any>?) in
            print("\n🔮👤 Result Request: \(response?.description ?? "nil") \n")
            completion(response)
        }
    }
    
//    // Requisição com upload de arquivo
//    @discardableResult
//    static func performRequestUpload( formData : @escaping (MultipartFormData?) -> Void, route : APIConfiguration, completion : @escaping (DataResponse<Any>?)-> Void) -> DataRequest {
//        return AF.upload(multipartFormData: { (response : MultipartFormData?) in formData(response) } , usingThreshold: UInt64.init(), with: route, interceptor: LogInterceptor()).validate().responseJSON(completionHandler: { (response : DataResponse<Any>?) in
//            print("\n🔮📂 Result Request: \(response?.description ?? "nil") \n")
//            completion(response)
//        })
//    }
    
    
}
