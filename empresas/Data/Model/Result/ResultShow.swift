//
//  ResultShow.swift
//  empresas
//
//  Created by Beatriz Teles Castro on 15/01/20.
//  Copyright © 2020 Beatriz Teles Castro. All rights reserved.
//

import Foundation

// MARK: - ResultShow
struct ResultShow: Codable {
    let enterpriseDetails: EnterpriseDetails
    let success: Bool
}

// MARK: - Enterprise
struct EnterpriseDetails: Codable {
    let id: Int
    let enterpriseName, enterpriseDescription, emailEnterprise, facebook: String
    let twitter, linkedin, phone: String
    let ownEnterprise: Bool
    let photo: String
    let value, shares, sharePrice, ownShares: Int
    let city, country: String
    let enterpriseType: EnterpriseType

    enum CodingKeys: String, CodingKey {
        case id
        case enterpriseName = "enterprise_name"
        case enterpriseDescription = "description"
        case emailEnterprise = "email_enterprise"
        case facebook, twitter, linkedin, phone
        case ownEnterprise = "own_enterprise"
        case photo, value, shares
        case sharePrice = "share_price"
        case ownShares = "own_shares"
        case city, country
        case enterpriseType = "enterprise_type"
    }
}
