//
//  ResultTeste.swift
//  empresas
//
//  Created by Beatriz Teles Castro on 15/01/20.
//  Copyright © 2020 Beatriz Teles Castro. All rights reserved.
//

import Foundation

// MARK: - ResultTeste
struct ResultTeste: Codable {
    let enterprises: [Enterpris]
}

// MARK: - Enterpris
struct Enterpris: Codable {
    let id: Int
    let emailEnterprise, facebook, twitter, linkedin: String?
    let phone: String?
    let ownEnterprise: Bool
    let enterpriseName: String
    let photo: String?
    let enterprisDescription, city: String
    let country: Country
    let value, sharePrice: Int
    let enterpriseType: EnterpriseType

    enum CodingKeys: String, CodingKey {
        case id
        case emailEnterprise = "email_enterprise"
        case facebook, twitter, linkedin, phone
        case ownEnterprise = "own_enterprise"
        case enterpriseName = "enterprise_name"
        case photo
        case enterprisDescription = "description"
        case city, country, value
        case sharePrice = "share_price"
        case enterpriseType = "enterprise_type"
    }
}

enum Country: String, Codable {
    case argentina = "Argentina"
    case chile = "Chile"
    case colombia = "Colombia"
    case unitedKingdom = "United Kingdom"
    case unitedStates = "United States"
}

