//
//  ResultEnterprisesIndex.swift
//  empresas
//
//  Created by Beatriz Teles Castro on 14/01/20.
//  Copyright © 2020 Beatriz Teles Castro. All rights reserved.
//

import Foundation

// MARK: - ResultEnterprisesIndex
struct ResultEnterprisesIndex: Codable, Error {
    let enterprises: [Enterprise]
}

// MARK: - Enterprise
struct Enterprise: Codable {
    let id: Int
    let emailEnterprise, facebook, twitter, linkedin: JSONNull?
    let phone: JSONNull?
    let ownEnterprise: Bool
    let enterpriseName: String
    let photo: JSONNull?
    let enterprisDescription, city, country: String
    let value, sharePrice: Int
    let enterpriseType: EnterpriseType

    enum CodingKeys: String, CodingKey {
        case id
        case emailEnterprise = "email_enterprise"
        case facebook, twitter, linkedin, phone
        case ownEnterprise = "own_enterprise"
        case enterpriseName = "enterprise_name"
        case photo
        case enterprisDescription = "description"
        case city, country, value
        case sharePrice = "share_price"
        case enterpriseType = "enterprise_type"
    }
}

// MARK: - EnterpriseType
struct EnterpriseType: Codable {
    let id: Int
    let enterpriseTypeName: String

    enum CodingKeys: String, CodingKey {
        case id
        case enterpriseTypeName = "enterprise_type_name"
    }
}

//// MARK: - Encode/decode helpers
//
//class JSONNull: Codable, Hashable {
//
//    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
//        return true
//    }
//
//    public var hashValue: Int {
//        return 0
//    }
//
//    public init() {}
//
//    public required init(from decoder: Decoder) throws {
//        let container = try decoder.singleValueContainer()
//        if !container.decodeNil() {
//            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
//        }
//    }
//
//    public func encode(to encoder: Encoder) throws {
//        var container = encoder.singleValueContainer()
//        try container.encodeNil()
//    }
//}
