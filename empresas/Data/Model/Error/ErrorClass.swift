////
////  ErrorClass.swift
////  empresas
////
////  Created by Beatriz Teles Castro on 14/01/20.
////  Copyright © 2020 Beatriz Teles Castro. All rights reserved.
////
//
//import Foundation
//import UIKit
//
//class ErrorClass {
//    
//    func getError(errorObject: ErrorLogin) -> String{
//        
//        var message = "Serviço indisponível no momento."
//        
//        let messageError = errorObject.localizedDescription.description {
//            message = messageError
//        }
//        
//        
//        //Se tiver array de erros
//        let errorFromArray = getListErrorArray(arrayErrors: errorObject.errors)
//        if let error = errorFromArray {
//            message = error
//        }
//        
//        //Se tiver um error code
//        if let error = errorObject.success {
//            return message + " (x\(error))"
//        }
//        
//        return message
//    }
//        
//        
//    func getListErrorArray(arrayErrors: [String]?) -> String? {
//        
//        if let arrayError = arrayErrors {
//            if arrayError.count > 1 {
//                return "Alguns erros foram encontrados. \(arrayError[0])"
//            } else {
//                return arrayError[0]
//            }
//        }
//        return nil
//    }
//
//}
