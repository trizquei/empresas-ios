//
//  ErrorLogin.swift
//  empresas
//
//  Created by Beatriz Teles Castro on 14/01/20.
//  Copyright © 2020 Beatriz Teles Castro. All rights reserved.
//

import Foundation

// MARK: - ErrorLogin
struct ErrorLogin: Codable, Error {
    let success: Bool
    let errors: [String]
}
