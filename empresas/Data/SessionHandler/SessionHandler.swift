//
//  SessionHandler.swift
//  empresas
//
//  Created by Beatriz Teles Castro on 14/01/20.
//  Copyright © 2020 Beatriz Teles Castro. All rights reserved.
//

import Foundation

class SessionHandler {
    
    
    // User object
    static func setUser(data : Investor) {
        // Encode
        let jsonEncoder = JSONEncoder()
        do {
            let jsonData = try jsonEncoder.encode(data)
            let jsonString = String(data: jsonData, encoding: .utf8)
            
            //Persistindo dados da sessão como uma string
            UserDefaults.standard.set(jsonString!, forKey: "session_key_user")
        }
        catch {
            debugPrint("Ocorreu um erro ao searilizar o objeto -> SessionHandle: setUser")
        }
    }
    static func removeUser() {
        UserDefaults.standard.removeObject(forKey: "session_key_user")
    }
    static func getUser() -> Investor? {
        let data = UserDefaults.standard.string(forKey: "session_key_user")
        if data != nil {
            if let jsonData = data!.data(using: .utf8){
                let decoder = JSONDecoder()
                let user = try! decoder.decode(Investor.self, from: jsonData)
                return user
            }
            debugPrint("Ocorreu um erro ao desearilizar o objeto -> SessionHandle: getUser")
            return nil
        }
        return nil
    }
    
    
    // User
    static func setUserId(data : String) {
        UserDefaults.standard.set(data, forKey: "session_key_user_id")
    }
    static func removeUserId() {
        UserDefaults.standard.removeObject(forKey: "session_key_user_id")
    }
    static func getUserId() -> String? {
        let data = UserDefaults.standard.string(forKey: "session_key_user_id")
        if data != nil {
            return data
        }
        debugPrint("Ocorreu um erro ao desearilizar o objeto -> SessionHandle: getUserId")
        return nil
    }
    
    // Username
    static func setUsername(data : String) {
        UserDefaults.standard.set(data, forKey: "session_key_username")
    }
    static func removeUsername() {
        UserDefaults.standard.removeObject(forKey: "session_key_username")
    }
    static func getUsername() -> String? {
        
        let data = UserDefaults.standard.string(forKey: "session_key_username")
        if data != nil {
            return data
        }
        debugPrint("Ocorreu um erro ao desearilizar o objeto -> SessionHandle: getUsername")
        return nil
    }
    
    // Password
    static func setPassword(data : String) {
        let sourceData = data.data(using: .utf8)!
        let enc = sourceData.base64EncodedData()
        
        UserDefaults.standard.set(enc, forKey: "session_key_password")
    }
    static func removePassword() {
        
        UserDefaults.standard.removeObject(forKey: "session_key_password")
        
    }
    static func getPassword() -> String? {
        
        let data = UserDefaults.standard.data(forKey: "session_key_password")
        
        if data != nil {
            let decodedData = Data(base64Encoded: data!)!
            let decodedString = String(data: decodedData, encoding: .utf8)
            
            return decodedString
        }
        debugPrint("Ocorreu um erro ao desearilizar o objeto -> SessionHandle: getPassword")
        return nil
    }
    
    
    // Fullname
    static func setFullname(data : String) {
        UserDefaults.standard.set(data, forKey: "session_key_fullname")
    }
    static func removeFullname() {
        UserDefaults.standard.removeObject(forKey: "session_key_fullname")
    }
    static func getFullname() -> String? {
        
        let data = UserDefaults.standard.string(forKey: "session_key_fullname")
        if data != nil {
            return data
        }
        debugPrint("Ocorreu um erro ao desearilizar o objeto -> SessionHandle: getFullName")
        return nil
    }

    
    // Token
    static func setToken(data : String) {
        UserDefaults.standard.set(data, forKey: "session_key_token")
    }
    static func removeToken() {
        UserDefaults.standard.removeObject(forKey: "session_key_token")
    }
    static func getToken() -> String? {
        
        let data = UserDefaults.standard.string(forKey: "session_key_token")
        if data != nil {
            return data
        }
        debugPrint("Ocorreu um erro ao desearilizar o objeto -> SessionHandle: getToken")
        return nil
    }
    
    // Refresh Token
    static func setRefreshToken(data : String) {
        UserDefaults.standard.set(data, forKey: "session_key_refresh_token")
    }
    static func removeRefreshToken() {
        UserDefaults.standard.removeObject(forKey: "session_key_refresh_token")
    }
    static func getRefreshToken() -> String? {
        
        let data = UserDefaults.standard.string(forKey: "session_key_refresh_token")
        if data != nil {
            return data
        }
        debugPrint("Ocorreu um erro ao desearilizar o objeto -> SessionHandle: getRefreshToken")
        return nil
    }
    
    // Client
    static func setClient(data : String) {
        UserDefaults.standard.set(data, forKey: "session_key_client")
    }
    static func removeClient() {
        UserDefaults.standard.removeObject(forKey: "session_key_client")
    }
    static func getClient() -> String? {
        
        let data = UserDefaults.standard.string(forKey: "session_key_client")
        if data != nil {
            return data
        }
        debugPrint("Ocorreu um erro ao desearilizar o objeto -> SessionHandle: getClient")
        return nil
    }
}
