//
//  TextFieldCustom.swift
//  empresas
//
//  Created by Beatriz Teles Castro on 14/01/20.
//  Copyright © 2020 Beatriz Teles Castro. All rights reserved.
//

import Foundation
import UIKit

class TextFieldCustom: UITextField {
    
    var padding = UIEdgeInsets(top: 16, left: 20, bottom: 16, right: 20)
    var bottomBorder = UIView()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.translatesAutoresizingMaskIntoConstraints = false
        bottomBorder = UIView.init(frame: CGRect(x: 0, y: 0, width: percentOfWidth(percente: 16), height: percentOfHeight(percente: 16)))
        bottomBorder.translatesAutoresizingMaskIntoConstraints = false
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.textColor = .background_ioasys
        self.backgroundColor = .white
        self.tintColor = .background_ioasys
        
        self.elevate(elevation: 2)
        self.layer.cornerRadius = 10
        
        
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.leftViewRect(forBounds: bounds)
        textRect.origin.x += 16
        return textRect
    }
    
    
    func setIconOnLeft(){
        //        let btnView = UIButton(type: .custom)
        //
        //        btnView.frame = CGRect(x: 0, y: 0, width: 60, height: 60)
        //        btnView.setImage(#imageLiteral(resourceName: "icEmail"), for: .normal)
        //
        //        btnView.contentMode = .scaleAspectFill
        //        btnView.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        //        leftView = btnView
        //        leftViewMode = .always
        //        leftView?.isHidden = false
        
        
        let imageView = UIImageView(image: #imageLiteral(resourceName: "icEmail"))
        imageView.frame = CGRect(x: 0, y: 0, width: 50, height: 60)
        imageView.translatesAutoresizingMaskIntoConstraints = true
        imageView.contentMode = .scaleAspectFill
        leftView = imageView
        leftViewMode = .always
        leftView?.isHidden = false
    }
    
    func setIconPasswordOnLeft(){
        
        let imageView = UIImageView(image: #imageLiteral(resourceName: "icCadeado"))
        imageView.frame = CGRect(x: 0, y: 0, width: 50, height: 60)
        imageView.translatesAutoresizingMaskIntoConstraints = true
        imageView.contentMode = .scaleAspectFill
        leftView = imageView
        leftViewMode = .always
        leftView?.isHidden = false
    }
    
    func setIconErrorRight(){
        self.layer.borderWidth = 2
        self.layer.borderColor = #colorLiteral(red: 0.9058823529, green: 0.2980392157, blue: 0.2352941176, alpha: 0.5)
        
    }
    func removeEffects(){
        self.layer.borderWidth = 0
        self.layer.borderColor = UIColor.gray.cgColor
    }
    func changeStateF(){
        rightView?.isHidden = false
    }
    func changeStateT(){
        rightView?.isHidden = true
    }
    
    func setPasswordShowButton(_ customButton : UIButton){
        let iconContainerView: UIView = UIView(frame:
            CGRect(x: 40, y: 30, width: 40, height: 40))
        iconContainerView.addSubview(customButton)
        rightView = iconContainerView
        rightViewMode = .always
    }
    
    func setPasswordNotShowButton(_ customButton : UIButton){
        let iconContainerView: UIView = UIView(frame:
            CGRect(x: 40, y: 30, width: 40, height: 40))
        iconContainerView.addSubview(customButton)
        //        rightView?.isHidden = false
        rightView = iconContainerView
        rightViewMode = .always
    }
    
    
}
