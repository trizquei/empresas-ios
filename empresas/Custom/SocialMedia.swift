//
//  SocialMedia.swift
//  empresas
//
//  Created by Beatriz Teles Castro on 16/01/20.
//  Copyright © 2020 Beatriz Teles Castro. All rights reserved.
//

import UIKit


class SocialMedia: UIView, CAAnimationDelegate {
    
    
    var delegate = UIViewController()
    
    var previousController : UIViewController?
    var nextController : UIViewController?
    
    var steps  = 3 //Quantidade de redes sociais
    
    var subTypePrevious : CATransitionSubtype = CATransitionSubtype.fromLeft
    var typePrevious : CATransitionType = CATransitionType.push
    
    var subTypeNext : CATransitionSubtype = CATransitionSubtype.fromRight
    
    var typeNext : CATransitionType = CATransitionType.push
    //MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .background_ioasys
        layer.cornerRadius = 20
        layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        addViewsOnStepView()
        setConstraintOnStepView()
        connectActionOnStepView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //MARK: - Funções
    
    
    /**
     Adicionando na View os componentes que são criados
     na #Components
     */
    
    private func addViewsOnStepView(){
        addSubview(stackView)
        stackView.addArrangedSubview(linkedinButton)
        stackView.addArrangedSubview(facebookButton)
        stackView.addArrangedSubview(twitterButton)
        stackView.addArrangedSubview(emailButton)
        addSubview(bottomView)
    }
    
    
    /**
     Adicionando constraints nos componentes referenciando 'self' como
     sendo a StepView e usando os outros componentes como referencia também
     */
    
    private func setConstraintOnStepView(){
        
        
        stackView.center(inView: self)
        linkedinButton.size(width: 50, height: 50)
        twitterButton.size(width: 50, height: 50)
        facebookButton.size(width: 50, height: 50)
        emailButton.size(width: 50, height: 50)
        
        bottomView.anchor(top: self.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 300, trailling: nil, leading: nil)
    }
    
    
    /**
     Atribuindo ações criadas no #Ações de Botões para os botões da StepView
     */
    
    private func connectActionOnStepView(){
        twitterButton.addTarget(self, action: #selector(goToPreviousPage), for: .touchDown)
        twitterButton.addTarget(self, action: #selector(goToNextPage), for: .touchDown)
        
        
        
    }
    
    
    
    //MARK: - Ações de Botões
    
    /**
     Nesta função, a navegação usa um `pop` para voltar pra Controller anterior a essa na pilha.
     
     - Caso seja necessario especificar um caminho de volta:
     - Usar a variavel de classe previousController
     
     - Caso seja necessario uma animação diferente ( ex.: CATransitionType.`fade` precisa
     - Na View Controller mandar o tipo ou subtipo que deseja para
     as variaveis de classe type e subType, respectivamente.
     
     #Delegate
     Referece a *controller* que esta controlando essa classe.
     **Exemplo:**
     Na função onde a StepView está sendo criada na *Controller*, deve-se incluir a atribuição do Delegate:
     `stepView.delegate = self`
     */
    
    @objc func goToPreviousPage(){
        let transition = CATransition.init()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction.init(name: CAMediaTimingFunctionName.default)
        transition.type = typePrevious
        transition.subtype = subTypePrevious
        transition.delegate = self
        //            delegate.view.endEditing(true)
        self.window?.layer.add(transition, forKey: kCATransition)
        
        delegate.navigationController?.popViewController(animated: false)
        
        
        
    }
    
    /**
     Nesta função, a navegação usa um `push` para a proxima Controller do fluxo.
     
     #Caminho:
     - Cria-se uma variavel de função que recebe o valor da variavel de classe *nextController*,
     - Após verificar se a variavel é vazia ou não, chama a navegação da delegate passando
     como **Parameter** a variavel de função
     */
    
    @objc func goToNextPage(){
        let next = nextController
        let transition = CATransition.init()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction.init(name: CAMediaTimingFunctionName.default)
        transition.type = typeNext
        transition.subtype = subTypeNext
        transition.delegate = self
        //        delegate.view.endEditing(true)
        if next != nil {
            self.window?.layer.add(transition, forKey: kCATransition)
            delegate.navigationController?.pushViewController(next!, animated: false)
        }
        
    }
    
    
    //MARK: - Componentes
    
    
    
    // MARK: - Button
    
    var twitterButton: UIButton = {
        let component = UIButton(type: .custom)
        
        component.setImage(#imageLiteral(resourceName: "icons8-twitter-80"), for: .normal)
        component.frame = CGRect(x: 0, y: 5, width: 60, height: 60)
        component.contentEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        component.isEnabled = false
        component.tintColor = .white
        return component
    }()
    
    var facebookButton: UIButton = {
        let component = UIButton(type: .custom)
        component.setImage(#imageLiteral(resourceName: "icons8-facebook-80"), for: .normal)
        component.contentEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        component.frame = CGRect(x: 0, y: 5, width: 60, height: 60)
        component.isEnabled = false
        component.tintColor = .white
        return component
    }()
    
    var linkedinButton: UIButton = {
        let component = UIButton(type: .custom)
        
        component.setImage(#imageLiteral(resourceName: "icons8-linkedin-80"), for: .normal)
        component.frame = CGRect(x: 0, y: 5, width: 60, height: 60)
        component.contentEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        component.isEnabled = false
        component.tintColor = .white
        return component
    }()
    
    var emailButton: UIButton = {
        let component = UIButton(type: .custom)
        
        component.setImage(#imageLiteral(resourceName: "icons8-email-sign-80"), for: .normal)
        component.frame = CGRect(x: 0, y: 5, width: 60, height: 60)
        component.contentEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        component.isEnabled = false
        component.tintColor = .white
        return component
    }()
    
    // MARK: - Stack View
    
    var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis  = NSLayoutConstraint.Axis.horizontal
        stackView.spacing   = 16
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    
    // MARK: - Bottom View
    
    var bottomView: UIView = {
        let component = UIView()
        component.backgroundColor = UIColor.blue
        component.translatesAutoresizingMaskIntoConstraints = false
        return component
    }()
    
    
    
}
