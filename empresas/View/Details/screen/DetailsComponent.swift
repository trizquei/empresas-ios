//
//  DetailsComponents.swift
//  empresas
//
//  Created by Beatriz Teles Castro on 15/01/20.
//  Copyright © 2020 Beatriz Teles Castro. All rights reserved.
//

import UIKit

class DetailsComponents {
  
    //MARK:- Labels
    var infoLabel: UILabel = {
        let component = UILabel()
        component.translatesAutoresizingMaskIntoConstraints = false
        component.numberOfLines = -1
        let text = "Name"
        
        guard let poppinsSemiBold = UIFont(name: "Poppins-SemiBold", size: 22) else {
            fatalError("""
            Failed to load the "Poppins-SemiBold" font.
            Make sure the font file is included in the project and the font name is spelled correctly.
        """)
        }
        
        let normalAttributteString = [.font: poppinsSemiBold, .foregroundColor: UIColor.background_ioasys] as [NSAttributedString.Key: Any]
        
        let attributedTitle = NSMutableAttributedString(string: text, attributes: normalAttributteString)
        component.attributedText = attributedTitle
        component.textAlignment = .center
        return component
    }()
    
    
    var descriptionLabel: UILabel = {
        let component = UILabel()
        component.translatesAutoresizingMaskIntoConstraints = false
        component.numberOfLines = -1
        let text = "Description"
        
        guard let poppinsRegular = UIFont(name: "Poppins-Regular", size: 18) else {
            fatalError("""
            Failed to load the "Poppins-Regular" font.
            Make sure the font file is included in the project and the font name is spelled correctly.
        """)
        }
        
        let normalAttributteString = [.font: poppinsRegular, .foregroundColor: UIColor.label_info] as [NSAttributedString.Key: Any]
        
        let attributedTitle = NSMutableAttributedString(string: text, attributes: normalAttributteString)
        component.attributedText = attributedTitle
        component.textAlignment = .justified
        return component
    }()
    
    var cityLabel: UILabel = {
        let component = UILabel()
        component.translatesAutoresizingMaskIntoConstraints = false
        component.numberOfLines = -1
        
        
        guard let sfRegular = UIFont(name: "SFUIText-Regular", size: 12) else {
            fatalError("""
                Failed to load the "SFUIText-Regular" font.
                Make sure the font file is included in the project and the font name is spelled correctly.
            """)
        }
        
        let title = "Cidade"
        let normalAttributteString = [.font: sfRegular, .foregroundColor: UIColor.gray] as [NSAttributedString.Key: Any]
        
        let attributedTitle = NSMutableAttributedString(string: title, attributes: normalAttributteString)
        
        component.attributedText = attributedTitle
        component.textAlignment = .center
        return component
    }()
    
    
    var countryLabel: UILabel = {
        let component = UILabel()
        component.translatesAutoresizingMaskIntoConstraints = false
        component.numberOfLines = -1
        
        
        guard let sfSemibold = UIFont(name: "SFUIText-Semibold", size: 12) else {
            fatalError("""
                Failed to load the "SFUIText-Semibold" font.
                Make sure the font file is included in the project and the font name is spelled correctly.
            """)
        }
        let title = "País"
        
        let normalAttributteString = [.font: sfSemibold, .foregroundColor: UIColor.darkGray] as [NSAttributedString.Key: Any]
        
        let attributedTitle = NSMutableAttributedString(string: title, attributes: normalAttributteString)
        
        component.attributedText = attributedTitle
        component.textAlignment = .center
        return component
    }()
    
    var typeLabel: UILabel = {
        let component = UILabel()
        component.translatesAutoresizingMaskIntoConstraints = false
        component.numberOfLines = -1
        let text = "Type"
        
        guard let poppinsLight = UIFont(name: "Poppins-Light", size: 18) else {
            fatalError("""
            Failed to load the "Poppins-Light" font.
            Make sure the font file is included in the project and the font name is spelled correctly.
        """)
        }
        
        let normalAttributteString = [.font: poppinsLight, .foregroundColor: UIColor.label_green] as [NSAttributedString.Key: Any]
        
        let attributedTitle = NSMutableAttributedString(string: text, attributes: normalAttributteString)
        component.attributedText = attributedTitle
        component.textAlignment = .center
        
        return component
    }()
    //MARK:- ImageView
    var enterpriseImageView : UIImageView = {
        let component = UIImageView ()
        component.translatesAutoresizingMaskIntoConstraints = false
        component.image = #imageLiteral(resourceName: "praia-da-graciosa")
        component.contentMode = .scaleAspectFill
        component.clipsToBounds = true
        return component
    }()
    
    
    //MARK:- ScrollView
    var scrollView : UIScrollView = {
        let screensize: CGRect = UIScreen.main.bounds
        let screenWidth = screensize.width
        let screenHeight = screensize.height
        let scroll = UIScrollView(frame: CGRect(x: 0, y: 0, width: 100, height: screenHeight))

        scroll.clipsToBounds = true
        scroll.contentSize = CGSize(width: 0, height: screenHeight + 100)
        scroll.autoresizingMask = .flexibleHeight
        scroll.showsHorizontalScrollIndicator = true
        return scroll
    }()

    
}
