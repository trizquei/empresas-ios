//
//  DetailsScreen.swift
//  empresas
//
//  Created by Beatriz Teles Castro on 15/01/20.
//  Copyright © 2020 Beatriz Teles Castro. All rights reserved.
//

import UIKit

final class DetailsScreen: UIView {
    
    private var components: DetailsComponents
    
    init(components: DetailsComponents, frame: CGRect) {
        self.components = components
        super.init(frame: frame)
        setupView()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView(){
        

        addSubview(components.infoLabel)
        addSubview(components.enterpriseImageView)
        
        addSubview(components.cityLabel)
        addSubview(components.countryLabel)
        
        addSubview(components.typeLabel)
        addSubview(components.scrollView)
        components.scrollView.addSubview(components.descriptionLabel)
        
    }
    
    private func setupConstraints(){
        
        components.enterpriseImageView.anchor(top: safeAreaLayoutGuide.topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: percentOfHeight(percente: 30), trailling: nil, leading: nil)
        
        components.infoLabel.anchor(top: components.enterpriseImageView.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 0, paddingRight: -5, width: 0, height: 0, trailling: nil, leading: nil)
        
        components.cityLabel.anchor(top: components.infoLabel.bottomAnchor, left: nil, bottom: nil, right: nil, paddingTop: 2, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0, trailling: nil, leading: nil)
        components.cityLabel.centerX(inView: self)
        
        components.countryLabel.centerX(inView: components.cityLabel)
        components.countryLabel.anchor(top: components.cityLabel.bottomAnchor, left: nil, bottom: nil, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0, trailling: nil, leading: nil)
        
        
        components.typeLabel.anchor(top: components.countryLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 25, paddingBottom: -10, paddingRight:0, width: 0, height: 0, trailling: nil, leading: nil)
        
        components.scrollView.anchor(top: components.typeLabel.bottomAnchor, left: leftAnchor, bottom: safeAreaLayoutGuide.bottomAnchor, right: rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: -10, paddingRight: -10, width: 0, height: 0, trailling: nil, leading: nil)
        
        components.descriptionLabel.anchor(top: components.scrollView.topAnchor, left: components.scrollView.leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 5, paddingLeft: 15, paddingBottom: 0, paddingRight: -15, width: 0, height: 0, trailling: nil, leading: nil)
        
    }
}
