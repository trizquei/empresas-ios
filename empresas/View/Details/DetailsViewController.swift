//
//  DetailsViewController.swift
//  empresas
//
//  Created by Beatriz Teles Castro on 15/01/20.
//  Copyright © 2020 Beatriz Teles Castro. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {
    
    private var components: DetailsComponents!
    private var contentScreen: DetailsScreen!
    var iphoneX = false
    
    var details : Enterpris?
    
    //MARK: - Instanciando StepView
    
    /**
     Criando o componente da StepView a partir de uma instancia da Classe, e atribuindo caracteristicas referentes a ViewController e Fluxo atual.
     
     - `Steps`: Quantos passos essa funcionalidade tem até ser completada pelo usuario
     - `actualStep`: Em qual dos passos o usuario está
     - `nexController`: Definindo aqui para qual tela ele será mandando após apertar em `stepView.nextButton`
     
     */
    
    let socialMedia: SocialMedia = {
        let sm = SocialMedia()
        sm.steps = 2
        
        sm.typeNext = CATransitionType.push
        sm.subTypeNext = CATransitionSubtype.fromLeft
        return sm
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setutContentScreen()
        setupStepView()
        self.view.backgroundColor = UIColor.background_bege
        updateText()
        
    }
    
    private func setutContentScreen(){
        
        components = DetailsComponents()
        
        contentScreen = DetailsScreen(components: components, frame: view.frame)
        view.addSubview(contentScreen)
        contentScreen.translatesAutoresizingMaskIntoConstraints = false
        
        var initialConstraints = [NSLayoutConstraint]()
        
        initialConstraints += NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[v0]-0-|",
                                                             options: NSLayoutConstraint.FormatOptions(),
                                                             metrics: nil,
                                                             views: ["v0" : contentScreen!])
        initialConstraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[v0]-0-|",
                                                             options: NSLayoutConstraint.FormatOptions(),
                                                             metrics: nil,
                                                             views: ["v0" : contentScreen!])
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                //                print("iPhone 5 or 5S or 5C")
                break
            case 1334:
                //                print("iPhone 6/6S/7/8")
                break
            case 1920, 2208:
                //                print("iPhone 6+/6S+/7+/8+")
                break
            case 2436:
                //                print("iPhone X, XS")
                iphoneX = true
                
            case 2688:
                //                print("iPhone XS Max")
                iphoneX = true
                
            case 1792:
                //                print("iPhone XR")
                iphoneX = true
                
            default:
                print("Unknown")
            }
        }
        NSLayoutConstraint.activate(initialConstraints)
        
        self.view.clipsToBounds = true
    }
    
    
    //MARK: - Configurar o StepView
    /**
     Essa função adiciona a instancia `stepView` na *screen* desta *controller* em tempo de execução, além de definir suas `constraints`
     
     - stepView.`addingCircleToArray`:
     A função da classe é chamada aqui e não no `init` dela, por conta da ordem de execução.
     Se for chamada aqui, é possível definir as configurações dos **passos** nesta controller, do contrario deveria ser na propria classe,
     e isso removeria o conceito de uma classe que se adaptaria aonde está sendo aplicada, tornando as quantidades de passos fixa.
     
     */
    
    private func setupStepView(){
        self.view.addSubview(socialMedia)
        socialMedia.delegate = self
        socialMedia.anchor(top: nil, left: self.view.leftAnchor, bottom: self.view.safeAreaLayoutGuide.bottomAnchor, right: self.view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 62, trailling: nil, leading: nil)
        
        
    }
    
    
    func updateText(){
        components.infoLabel.text = details?.enterpriseName
        components.descriptionLabel.text = "\t\(details?.enterprisDescription ?? "Não há descrição. ")"
        components.cityLabel.text = details?.city.capitalized
        let country = "\(details!.country)"
        components.countryLabel.text = country.capitalized
        components.typeLabel.text = details?.enterpriseType.enterpriseTypeName
        
        components.enterpriseImageView.image = GenerateImage.getImageToShow(nameType: details?.enterpriseType.enterpriseTypeName ?? "Software")
        
        
    }
    
}
