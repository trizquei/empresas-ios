//
//  HomeViewController.swift
//  empresas-app
//
//  Created by Beatriz Teles Castro on 13/01/20.
//  Copyright © 2020 Beatriz Teles Castro. All rights reserved.
//

import UIKit
import Lottie

class HomeViewController: UIViewController, CAAnimationDelegate {
    
    //MARK:- Variaveis
    private var components: HomeComponents!
    private var contentScreen: HomeScreen!
    
    
    let loadingAnimationView = AnimationView()
    var keyboardHeightCalled : CGFloat = 0
    var iphoneX = false
    
    // MARK: - Model
    var viewModel: LoginViewModel = LoginViewModel() {
        
        willSet{
            viewModel.viewDelegate = nil
        }
        didSet{
            viewModel.viewDelegate = self
        }
    }
    
    //MARK:- Configuração de tela
    override func viewDidLoad() {
        super.viewDidLoad()
        setutContentScreen()
        
        self.navigationController?.navigationBar.isHidden = true
        components.emailTextField.delegate = self
        components.passwordTextField.delegate = self
        
        components.singInButton.addTarget(self, action: #selector(segueToDash), for: .touchDown)
        viewModel = LoginViewModel()
//        startAnimation()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        view.endEditing(true)
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    private func setutContentScreen(){
        
        components = HomeComponents()
        
        contentScreen = HomeScreen(components: components, frame: view.frame)
        view.addSubview(contentScreen)
        contentScreen.translatesAutoresizingMaskIntoConstraints = false
        
        var initialConstraints = [NSLayoutConstraint]()
        
        initialConstraints += NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[v0]-0-|",
                                                             options: NSLayoutConstraint.FormatOptions(),
                                                             metrics: nil,
                                                             views: ["v0" : contentScreen!])
        initialConstraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[v0]-0-|",
                                                             options: NSLayoutConstraint.FormatOptions(),
                                                             metrics: nil,
                                                             views: ["v0" : contentScreen!])
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                //                print("iPhone 5 or 5S or 5C")
                break
            case 1334:
                //                print("iPhone 6/6S/7/8")
                break
            case 1920, 2208:
                //                print("iPhone 6+/6S+/7+/8+")
                break
            case 2436:
                //                print("iPhone X, XS")
                iphoneX = true
                
            case 2688:
                //                print("iPhone XS Max")
                iphoneX = true
                
            case 1792:
                //                print("iPhone XR")
                iphoneX = true
                
            default:
                print("Unknown")
            }
        }
        NSLayoutConstraint.activate(initialConstraints)
        
        self.view.clipsToBounds = true
    }
    
    //MARK:- Ação de Botão
    
    @objc func segueToDash(){
        let email = components.emailTextField.text ?? ""
        let password = components.passwordTextField.text ?? ""
        viewModel.submit(email: email, password: password)
        
        
    }
    
    //MARK: - Keyboard
    /**
       As proximas duas ações são chamadas na `NotificationCenter` ou seja, essa controller estara responsavel em observar as mudanças de estado do
     Teclado (Keyboard) nos seus dois estados - ativado ou inativo.
     
     #O que ela faz:
        Ela aplica alterações nas posições de alguns componentes. Como por exemplo, levantar o `CardView` quando o teclado estiver ativado, evitando assim que os componentes
     dentro dele não fiquem tampados pelo teclado.
     
     #Motivo pelo qual chama-la na NotificationCenter:
        A notificação acompanharia todas as alterações importantes na tela, sendo chamada na `viewDidAppear` e desabilitada na
     `viewDidAppear` evitando que ela permaneça ativado mudanças de layout em momentos que não deveria
                 
     
     */
    
    @objc func keyboardWillChange(){
        if keyboardHeightCalled != 0 {
            print("🚨keyboardHeightCalled/3", keyboardHeightCalled/3)
            self.components.singInButton.frame.origin.y += keyboardHeightCalled
            self.components.emailTextField.frame.origin.y += keyboardHeightCalled/3
            self.components.passwordTextField.frame.origin.y += keyboardHeightCalled/3

            self.components.infoLabel.frame.origin.y += 20
            self.components.infoLabel.alpha = 1
            keyboardHeightCalled = 0
        }

    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            
            
            if keyboardHeightCalled != 0 {
                let difference = keyboardHeight - keyboardHeightCalled
                self.components.singInButton.frame.origin.y -= difference
                self.components.emailTextField.frame.origin.y -= difference/3
                self.components.passwordTextField.frame.origin.y -= difference/3
            } else {
                 self.components.singInButton.frame.origin.y -= keyboardHeight
                 self.components.emailTextField.frame.origin.y -= keyboardHeight/3
                 self.components.passwordTextField.frame.origin.y -= keyboardHeight/3
                self.components.infoLabel.frame.origin.y -= 20
                self.components.infoLabel.alpha = 0
                
            }
            
            
            keyboardHeightCalled = keyboardHeight
            
            
            
        }
        
    }
    
    
    //MARK:- Funções
    
    func startAnimation(){
        loadingAnimationView.animation = Animation.named("loading_colors")

        components.lottieView.addSubview(loadingAnimationView)

        loadingAnimationView.translatesAutoresizingMaskIntoConstraints = false


        loadingAnimationView.center(inView: components.lottieView)

        loadingAnimationView.heightAnchor.constraint(equalToConstant: 250).isActive = true
        loadingAnimationView.widthAnchor.constraint(equalToConstant: 250).isActive = true

        loadingAnimationView.loopMode = LottieLoopMode.loop
        loadingAnimationView.animationSpeed = CGFloat(1)
        loadingAnimationView.play()
    }
    
    
    
}

extension HomeViewController: LoginViewModelDelegate {
    func errorMessageDidChange(message: String) {
        print("Message: \(message)")
        let alert = UIAlertController(title: "Ops, algo de errado aconteceu!", message: "Os dados inseridos são invalidos. Tente Novamente.", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func signinSuccess(email: String, password: String) {
        print("email: \(email), \nsenha: \(password)")

        let transition = CATransition.init()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction.init(name: CAMediaTimingFunctionName.default)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        transition.delegate = self
        self.view.endEditing(true)
        self.view.window?.layer.add(transition, forKey: kCATransition)
        self.navigationController?.pushViewController(DashViewController(), animated: false)
    }
    
    func startLoading() {
        print("💿💿Loading...📀📀")
        components.emailTextField.isEnabled = false
        components.passwordTextField.isEnabled = false
        components.singInButton.isEnabled = false
        self.view.endEditing(true)
        UIView.animate(withDuration: 0.3) {
            self.components.singInButton.alpha = 0.4
        }
        
        components.lottieView.alpha = 1
        startAnimation()
    }
    
    func stopLoading() {
        print("🚫🛑Stop Loading!🛑")
        components.emailTextField.isEnabled = true
        components.passwordTextField.isEnabled = true
        components.singInButton.isEnabled = true
        UIView.animate(withDuration: 0.3) {
            self.components.singInButton.alpha = 1
        }
        loadingAnimationView.stop()
        components.lottieView.alpha = 0
    }
    
    
}

extension HomeViewController: UITextFieldDelegate {
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if textField.text == "" {
            components.singInButton.isEnabled = false
            UIView.animate(withDuration: 0.3) {
                self.components.singInButton.alpha = 0.4
            }
        } else {
            if textField == components.emailTextField {
                if components.passwordTextField.text != "" {
                    
                    components.singInButton.isEnabled = true
                    UIView.animate(withDuration: 0.3) {
                        self.components.singInButton.alpha = 1
                    }
                }
            } else {
                if components.emailTextField.text != "" {
                    
                    components.singInButton.isEnabled = true
                    UIView.animate(withDuration: 0.3) {
                        self.components.singInButton.alpha = 1
                    }
                }
            }
        }
    }
}
