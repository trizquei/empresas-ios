//
//  HomeComponents.swift
//  empresas-app
//
//  Created by Beatriz Teles Castro on 13/01/20.
//  Copyright © 2020 Beatriz Teles Castro. All rights reserved.
//

import UIKit


class HomeComponents {
    
    //MARK:- Background
    var bgGradient: CAGradientLayer = {
        let component = CAGradientLayer()
        component.backgroundColor = UIColor.background_bege.cgColor

        return component
    }()
    
    
    //MARK:- Labels
    
    
    var welcomeLabel: UILabel = {
        let component = UILabel()
        component.translatesAutoresizingMaskIntoConstraints = false
        component.numberOfLines = -1
        let text = "BEM-VINDO AO EMPRESA"
        
        guard let robotoBold = UIFont(name: "Roboto-Bold", size: 16) else {
            fatalError("""
                Failed to load the "Roboto-Bold" font.
                Make sure the font file is included in the project and the font name is spelled correctly.
            """)
        }
        
        let normalAttributteString = [.font: robotoBold, .foregroundColor: UIColor.label_info] as [NSAttributedString.Key: Any]
        
        let attributedTitle = NSMutableAttributedString(string: text, attributes: normalAttributteString)
        component.attributedText = attributedTitle
        component.textAlignment = .center
        return component
    }()
    
    var infoLabel: UILabel = {
        let component = UILabel()
        component.translatesAutoresizingMaskIntoConstraints = false
        component.numberOfLines = -1
        let text = "Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan."
        
        guard let sfRegular = UIFont(name: "SFUIText-Regular", size: 15) else {
            fatalError("""
                Failed to load the "SFUIText-Regular" font.
                Make sure the font file is included in the project and the font name is spelled correctly.
            """)
        }
        
        let normalAttributteString = [.font: sfRegular, .foregroundColor: UIColor.label_info] as [NSAttributedString.Key: Any]
        
        let attributedTitle = NSMutableAttributedString(string: text, attributes: normalAttributteString)
        component.attributedText = attributedTitle
        component.textAlignment = .center
        
        return component
    }()
    
    //MARK:- ImageView
    
    var logoImageView: UIImageView = {
        let component = UIImageView(image: #imageLiteral(resourceName: "logoHome.png"))
        component.translatesAutoresizingMaskIntoConstraints = false
        component.contentMode = .scaleAspectFit
        component.tintColor = .label_ioasys
        return component
    }()
    
    
    //MARK:- TextField
    
    var emailTextField: TextFieldCustom = {
        let component = TextFieldCustom()
        component.translatesAutoresizingMaskIntoConstraints = false
        component.padding = UIEdgeInsets(top: 16, left: 50, bottom: 16, right: 20)
        guard let sfRegular = UIFont(name: "SFUIText-Regular", size: 15) else {
            fatalError("""
                Failed to load the "SFUIText-Regular" font.
                Make sure the font file is included in the project and the font name is spelled correctly.
            """)
        }
        
        component.attributedPlaceholder = NSAttributedString (string: NSLocalizedString(NSLocalizedString("E-mail", comment: ""), comment: "E-mail do Usuario"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray.withAlphaComponent(0.3), NSAttributedString.Key.font: sfRegular] )
        component.font = sfRegular
        component.keyboardType = .emailAddress
        
        component.autocapitalizationType = UITextAutocapitalizationType.none
        component.autocorrectionType = .no
        component.textAlignment = .left

        return component
    }()
    
    
    var passwordTextField: TextFieldCustom = {
        let component = TextFieldCustom()
        
        component.padding = UIEdgeInsets(top: 16, left: 45, bottom: 16, right: 20)
        guard let sfRegular = UIFont(name: "SFUIText-Regular", size: 15) else {
            fatalError("""
                Failed to load the "SFUIText-Regular" font.
                Make sure the font file is included in the project and the font name is spelled correctly.
            """)
        }
        
        component.translatesAutoresizingMaskIntoConstraints = false
        component.attributedPlaceholder = NSAttributedString (string: NSLocalizedString("Senha", comment: "Senha"), attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray.withAlphaComponent(0.3), NSAttributedString.Key.font: sfRegular])
        component.isSecureTextEntry = true
        component.font = sfRegular
        component.textAlignment = .left
        return component
    }()

    //MARK:- Button
    
    
    var singInButton: UIButton = {
        let component = UIButton(frame: .zero)
        component.backgroundColor = UIColor.background_button_green
        
        guard let sfSemiBold = UIFont(name: "SFUIText-Semibold", size: 20) else {
            fatalError("""
                Failed to load the "SFUIText-Semibold" font.
                Make sure the font file is included in the project and the font name is spelled correctly.
            """)
        }
        
        component.setAttributedTitle(NSAttributedString(string: NSLocalizedString("Entrar", comment: ""), attributes: [.font: sfSemiBold, .foregroundColor: UIColor.white]), for: .normal)
        component.translatesAutoresizingMaskIntoConstraints = false
        component.layer.cornerRadius = 10
        component.isEnabled = false
        component.alpha = 0.4
        return component

    }()
    
    
    //MARK:- View
    
    var lottieView: UIView = {
        let component = UIView()
        component.translatesAutoresizingMaskIntoConstraints = false
        component.backgroundColor = .clear
        return component
    }()
    
}
