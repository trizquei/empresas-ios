//
//  HomeScreen.swift
//  empresas-app
//
//  Created by Beatriz Teles Castro on 13/01/20.
//  Copyright © 2020 Beatriz Teles Castro. All rights reserved.
//

import UIKit

final class HomeScreen: UIView {
    
    private var components: HomeComponents
    
    init(components: HomeComponents, frame: CGRect) {
        self.components = components
        super.init(frame: frame)
        setupView()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func setupView(){
        components.bgGradient.frame = (bounds)
        layer.addSublayer(components.bgGradient)
        
        //Logo
        addSubview(components.logoImageView)
        
        //Title
        addSubview(components.welcomeLabel)
        addSubview(components.infoLabel)
        
        //E-mail
        addSubview(components.emailTextField)
        components.emailTextField.setIconOnLeft()
        
        //Password
        addSubview(components.passwordTextField)
        components.passwordTextField.setIconPasswordOnLeft()
        
        //SignIn
        addSubview(components.singInButton)
        
        
        addSubview(components.lottieView)
    }
    
    private func setupConstraints(){
        
        
        components.logoImageView.anchor(top: self.safeAreaLayoutGuide.topAnchor, left: nil, bottom: nil, right: nil, paddingTop: 49, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0, trailling: nil, leading: nil)
        
        components.logoImageView.centerX(inView: self)
        
        components.welcomeLabel.anchor(top: components.logoImageView.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, paddingTop: 40, paddingLeft: 55, paddingBottom: 0, paddingRight: -55, width: 0, height: 0, trailling: nil, leading: nil)
        
        components.infoLabel.anchor(top: components.welcomeLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, paddingTop: 19, paddingLeft: 55, paddingBottom: 0, paddingRight: -55, width: 0, height: 0, trailling: nil, leading: nil)
        
        components.emailTextField.anchor(top: components.infoLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, paddingTop: 65, paddingLeft: 25, paddingBottom: 0, paddingRight: -25, width: 0, height: 50, trailling: nil, leading: nil)
        
        components.passwordTextField.anchor(top: components.emailTextField.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, paddingTop: percentOfHeight(percente: 2), paddingLeft: 25, paddingBottom: 0, paddingRight: -25, width: 0, height: 50, trailling: nil, leading: nil)
        
        components.singInButton.anchor(top: nil, left: self.leftAnchor, bottom: self.safeAreaLayoutGuide.bottomAnchor, right: self.rightAnchor, paddingTop: 0, paddingLeft: 25, paddingBottom: -21, paddingRight: -25, width: 0, height: 62, trailling: nil, leading: nil)
        
        components.lottieView.anchor(top: components.passwordTextField.bottomAnchor, left: leftAnchor, bottom: components.singInButton.topAnchor, right: rightAnchor, paddingTop: 50, paddingLeft: 20, paddingBottom: -50, paddingRight: -20, width: 0, height: 0, trailling: nil, leading: nil)
    }
}
