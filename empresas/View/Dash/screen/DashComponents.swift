//
//  DashComponents.swift
//  empresas
//
//  Created by Beatriz Teles Castro on 14/01/20.
//  Copyright © 2020 Beatriz Teles Castro. All rights reserved.
//

import UIKit

class DashComponents {
    
    //MARK:- Labels
    var notFoundLabel: UILabel = {
        let component = UILabel()
        component.translatesAutoresizingMaskIntoConstraints = false
        component.numberOfLines = -1
        let text = "Nada encontrado"
        
        guard let sfRegular = UIFont(name: "SFUIText-Regular", size: 18) else {
            fatalError("""
                Failed to load the "SFUIText-Regular" font.
                Make sure the font file is included in the project and the font name is spelled correctly.
            """)
        }
        
        let normalAttributteString = [.font: sfRegular, .foregroundColor: UIColor.label_info] as [NSAttributedString.Key: Any]
        
        let attributedTitle = NSMutableAttributedString(string: text, attributes: normalAttributteString)
        component.attributedText = attributedTitle
        component.textAlignment = .center
        component.alpha = 0
        return component
    }()
    
    
    //MARK:- UITableView
    
    var enterpriseTableView : UITableView = {
        let component = UITableView()
        component.translatesAutoresizingMaskIntoConstraints = false
        component.backgroundColor = .clear
        component.separatorStyle = .none
        component.showsVerticalScrollIndicator = false
        return component
    }()
    

    
}


