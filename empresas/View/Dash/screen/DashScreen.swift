//
//  DashScreen.swift
//  empresas
//
//  Created by Beatriz Teles Castro on 14/01/20.
//  Copyright © 2020 Beatriz Teles Castro. All rights reserved.
//

import UIKit

final class DashScreen: UIView {
    
    private var components: DashComponents
    
    init(components: DashComponents, frame: CGRect) {
        self.components = components
        super.init(frame: frame)
        setupView()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView(){
        addSubview(components.notFoundLabel)
        addSubview(components.enterpriseTableView)
    }
    
    private func setupConstraints(){
        
        components.notFoundLabel.center(inView: self)
        
        
        components.enterpriseTableView.anchor(top: self.safeAreaLayoutGuide.topAnchor, left: leftAnchor, bottom: self.safeAreaLayoutGuide.bottomAnchor, right: rightAnchor, paddingTop: percentOfHeight(percente: 2), paddingLeft: 15, paddingBottom: 0, paddingRight: -15, width: 0, height: 0, trailling: nil, leading: nil)
    }
}
