//
//  DashTableViewCell.swift
//  empresas
//
//  Created by Beatriz Teles Castro on 14/01/20.
//  Copyright © 2020 Beatriz Teles Castro. All rights reserved.
//

import UIKit
import Lottie

class DashTableViewCell: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        
        
        contentView.addSubview(principalView)
        principalView.addSubview(enterpriseImageView)
        
        principalView.addSubview(labelView)
        principalView.addSubview(itemLabel)
    
        contentView.addSubview(typeLabel)
        principalView.addSubview(cityLabel)
        principalView.addSubview(countryLabel)
        
        principalView.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, paddingTop: 15, paddingLeft: 10, paddingBottom: -15, paddingRight: -10, width: 0, height: 0, trailling: nil, leading: nil)
        
        
        enterpriseImageView.anchor(top: principalView.topAnchor, left: principalView.leftAnchor, bottom: principalView.bottomAnchor, right: principalView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0, trailling: nil, leading: nil)
        
        typeLabel.anchor(top: self.topAnchor, left: nil, bottom: nil, right: self.principalView.rightAnchor, paddingTop: 20, paddingLeft: 0, paddingBottom: 0, paddingRight: -10, width: 0, height: 0, trailling: nil, leading: nil)
        
        labelView.anchor(top: nil, left: principalView.leftAnchor, bottom: principalView.bottomAnchor, right: principalView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 50, trailling: nil, leading: nil)
        
        itemLabel.anchor(top: labelView.topAnchor, left: labelView.leftAnchor, bottom: nil, right: labelView.rightAnchor, paddingTop: 5, paddingLeft: 35, paddingBottom: 0, paddingRight: -10, width: 0, height: 0, trailling: nil, leading: nil)
        
        cityLabel.anchor(top: itemLabel.bottomAnchor, left: labelView.leftAnchor, bottom: nil, right: nil, paddingTop: 2, paddingLeft: 35, paddingBottom: 0, paddingRight: -20, width: 0, height: 0, trailling: nil, leading: nil)
        
        countryLabel.centerY(inView: cityLabel)
        countryLabel.anchor(top: nil, left: cityLabel.rightAnchor, bottom: nil, right: labelView.rightAnchor, paddingTop: 0, paddingLeft: 2, paddingBottom: 0, paddingRight: -20, width: 0, height: 0, trailling: nil, leading: nil)
        
        
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var enterpriseImageView : UIImageView = {
        let element = UIImageView ()
        element.translatesAutoresizingMaskIntoConstraints = false
        element.image = #imageLiteral(resourceName: "logoHome")
        element.contentMode = .scaleAspectFill
        return element
    }()
    
    var itemLabel: UILabel = {
        let component = UILabel()
        component.translatesAutoresizingMaskIntoConstraints = false
        component.numberOfLines = 1
        
        
        component.textAlignment = .left
        component.minimumScaleFactor = 10
        component.adjustsFontSizeToFitWidth = true
        component.adjustsFontForContentSizeCategory = true
        guard let sfSemiBold = UIFont(name: "SFUIText-Semibold", size: 20) else {
            fatalError("""
                Failed to load the "SFUIText-Semibold" font.
                Make sure the font file is included in the project and the font name is spelled correctly.
            """)
        }
        let title = "Empresa"
        
        let normalAttributteString = [.font: sfSemiBold, .foregroundColor: UIColor.white] as [NSAttributedString.Key: Any]
        
        let attributedTitle = NSMutableAttributedString(string: title, attributes: normalAttributteString)
        
        component.attributedText = attributedTitle
        return component
    }()
    
    var typeLabel: UILabel = {
        let component = UILabel()
        component.translatesAutoresizingMaskIntoConstraints = false
        component.backgroundColor = UIColor.white.withAlphaComponent(0.8)
        component.showsLargeContentViewer = true
        component.numberOfLines = -1
        let text = "tipo"
        
        guard let poppinsSemiBold = UIFont(name: "Poppins-SemiBold", size: 17) else {
            fatalError("""
            Failed to load the "Poppins-SemiBold" font.
            Make sure the font file is included in the project and the font name is spelled correctly.
        """)
        }
        
        let normalAttributteString = [.font: poppinsSemiBold, .foregroundColor: UIColor.background_ioasys] as [NSAttributedString.Key: Any]
        
        let attributedTitle = NSMutableAttributedString(string: text, attributes: normalAttributteString)
        component.attributedText = attributedTitle
        component.textAlignment = .right
        return component
    }()
    
    var cityLabel: UILabel = {
        let component = UILabel()
        component.translatesAutoresizingMaskIntoConstraints = false
        component.numberOfLines = -1
        
        
        guard let sfRegular = UIFont(name: "SFUIText-Regular", size: 12) else {
            fatalError("""
                Failed to load the "SFUIText-Regular" font.
                Make sure the font file is included in the project and the font name is spelled correctly.
            """)
        }
        
        let title = "Cidade, "
        let normalAttributteString = [.font: sfRegular, .foregroundColor: UIColor.white] as [NSAttributedString.Key: Any]
        
        let attributedTitle = NSMutableAttributedString(string: title, attributes: normalAttributteString)
        
        component.attributedText = attributedTitle
        component.textAlignment = .left
        return component
    }()
    
    var countryLabel: UILabel = {
        let component = UILabel()
        component.translatesAutoresizingMaskIntoConstraints = false
        component.numberOfLines = -1
        
        
        guard let sfSemibold = UIFont(name: "SFUIText-Semibold", size: 12) else {
            fatalError("""
                Failed to load the "SFUIText-Semibold" font.
                Make sure the font file is included in the project and the font name is spelled correctly.
            """)
        }
        let title = "País"
        
        let normalAttributteString = [.font: sfSemibold, .foregroundColor: UIColor.white] as [NSAttributedString.Key: Any]
        
        let attributedTitle = NSMutableAttributedString(string: title, attributes: normalAttributteString)
        
        component.attributedText = attributedTitle
        component.textAlignment = .left
        return component
    }()
    
    var principalView: UIView = {
        let component = UIView()
        component.translatesAutoresizingMaskIntoConstraints = false
        component.backgroundColor = .green
        component.layer.cornerRadius = 10
        component.clipsToBounds = true
        return component
    }()
    
    var labelView: UIView = {
        let component = UIView()
        component.translatesAutoresizingMaskIntoConstraints = false
        component.backgroundColor = UIColor.background_button_green
        component.clipsToBounds = true
        component.layer.cornerRadius = 10
        component.alpha = 0.5
        return component
    }()
    
    
//    //MARK: - Lottie
//    var imageLottieView: UIView = {
//        let component = UIView()
//        component.translatesAutoresizingMaskIntoConstraints = false
//        component.backgroundColor = .background_bege
//        let animationView = AnimationView(name: "loading_image")
//        animationView.frame = CGRect(x: 0, y: 0, width: 200, height: 400)
//        animationView.contentMode = .scaleAspectFill
//        animationView.loopMode = LottieLoopMode.loop
//        animationView.animationSpeed = 0.8
//        component.addSubview(animationView)
//
//        component.alpha = 1
//        animationView.play()
//        return component
//    }()
}
