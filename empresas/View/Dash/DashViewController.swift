//
//  DashViewController.swift
//  empresas
//
//  Created by Beatriz Teles Castro on 14/01/20.
//  Copyright © 2020 Beatriz Teles Castro. All rights reserved.
//

import UIKit


struct DashFunctionality {
    var nome : String
    var balance: Int
    var description : String
    
    
}
class DashViewController: UIViewController, CAAnimationDelegate{
    
    //MARK:- Variaveis
    private var components: DashComponents!
    private var contentScreen: DashScreen!
    
    var iphoneX = false
    var cellId = "cellId"
    
    var filterdTerms : [Enterpris] = []
    var enterprisesListAPI : [Enterpris] = []
    
    
    // MARK: - Model
    var viewModel: DashViewModel = DashViewModel() {
        
        willSet{
            viewModel.viewDelegate = nil
        }
        didSet{
            viewModel.viewDelegate = self
        }
    }
    //MARK:- Configuração de tela
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.background_bege
        
        setutContentScreen()
        setupNavBar()
        addNavBarImage()
        viewModel = DashViewModel()
        components.enterpriseTableView.register(DashTableViewCell.self, forCellReuseIdentifier: "cellId")
        components.enterpriseTableView.dataSource = self
        components.enterpriseTableView.delegate = self
        feedingList()
    }
    
    private func setutContentScreen(){
        
        components = DashComponents()
        
        contentScreen = DashScreen(components: components, frame: view.frame)
        view.addSubview(contentScreen)
        contentScreen.translatesAutoresizingMaskIntoConstraints = false
        
        var initialConstraints = [NSLayoutConstraint]()
        
        initialConstraints += NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[v0]-0-|",
                                                             options: NSLayoutConstraint.FormatOptions(),
                                                             metrics: nil,
                                                             views: ["v0" : contentScreen!])
        initialConstraints += NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[v0]-0-|",
                                                             options: NSLayoutConstraint.FormatOptions(),
                                                             metrics: nil,
                                                             views: ["v0" : contentScreen!])
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                //                print("iPhone 5 or 5S or 5C")
                break
            case 1334:
                //                print("iPhone 6/6S/7/8")
                break
            case 1920, 2208:
                //                print("iPhone 6+/6S+/7+/8+")
                break
            case 2436:
                //                print("iPhone X, XS")
                iphoneX = true
                
            case 2688:
                //                print("iPhone XS Max")
                iphoneX = true
                
            case 1792:
                //                print("iPhone XR")
                iphoneX = true
                
            default:
                print("Unknown")
            }
        }
        NSLayoutConstraint.activate(initialConstraints)
        
        self.view.clipsToBounds = true
    }
    
    private func setupNavBar(){
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.barTintColor = .background_ioasys
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 80)
        
        self.navigationController?.navigationBar.barStyle = .black
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK:- Funções
    func addNavBarImage() {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "logoHome"))
        imageView.frame = CGRect(x: 0, y: 0, width: 100, height: 25)
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = .white
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 25))
        
        titleView.addSubview(imageView)
        titleView.backgroundColor = .clear
        self.navigationItem.titleView = titleView
    }
    
    
    func feedingList(){
        viewModel.feedingList()
    }
    
    //MARK:- Ação de Botão

    
}

extension DashViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return enterprisesListAPI.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = components.enterpriseTableView.dequeueReusableCell(withIdentifier: "cellId", for: indexPath) as? DashTableViewCell
        
        let itemToShow = enterprisesListAPI[indexPath.row]
        
        //font
        guard let sfSemiBold = UIFont(name: "SFUIText-Semibold", size: 20) else {
            fatalError("""
                    Failed to load the "SFUIText-Semibold" font.
                    Make sure the font file is included in the project and the font name is spelled correctly.
                """)
        }
        
        let itemActiveTitleAttributteString = [.font: sfSemiBold, .foregroundColor: UIColor.white] as [NSAttributedString.Key: Any]
        
        //ItemLabel
        
        let itemActiveAttributedTitle = NSMutableAttributedString(string: itemToShow.enterpriseName, attributes: itemActiveTitleAttributteString)
        
        cell?.itemLabel.minimumScaleFactor = 10
        cell?.itemLabel.adjustsFontSizeToFitWidth = true
        cell?.itemLabel.adjustsFontForContentSizeCategory = true
        cell?.itemLabel.attributedText = itemActiveAttributedTitle
        
        cell?.itemLabel.alpha = 1
        cell?.backgroundColor = .clear
        
        cell?.enterpriseImageView.image = GenerateImage.getImageToShow(nameType: itemToShow.enterpriseType.enterpriseTypeName)
        
        
        //ads
        guard let sfRegular = UIFont(name: "SFUIText-Regular", size: 12) else {
            fatalError("""
                    Failed to load the "SFUIText-Regular" font.
                    Make sure the font file is included in the project and the font name is spelled correctly.
                """)
        }
        
        
        
        let cityAttributteString = [.font: sfRegular, .foregroundColor: UIColor.white] as [NSAttributedString.Key: Any]
        
        let cityAttributedTitle = NSMutableAttributedString(string: "\(itemToShow.city.capitalized),", attributes: cityAttributteString)
        
        cell?.cityLabel.attributedText = cityAttributedTitle
        
        //Country
        
        guard let sfSemibold = UIFont(name: "SFUIText-Semibold", size: 12) else {
            fatalError("""
                    Failed to load the "SFUIText-Semibold" font.
                    Make sure the font file is included in the project and the font name is spelled correctly.
                """)
        }
        
        let title = "\(itemToShow.country)"
        let countryAttributteString = [.font: sfSemibold, .foregroundColor: UIColor.white] as [NSAttributedString.Key: Any]
        let countryAttributedTitle = NSMutableAttributedString(string: title.capitalized, attributes: countryAttributteString)
        
        cell?.countryLabel.attributedText = countryAttributedTitle
        
        
        
        
        cell?.typeLabel.text = itemToShow.enterpriseType.enterpriseTypeName
        return cell!
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return components.enterpriseTableView.percentOfHeight(percente: 40)
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        tabBarController?.tabBar.isHidden = false
        let transition = CATransition.init()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction.init(name: CAMediaTimingFunctionName.default)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        transition.delegate = self
        self.view.endEditing(true)
        self.view.window?.layer.add(transition, forKey: kCATransition)
        self.performSegue(withIdentifier: "enterpriseList", sender: self, indexPath: indexPath)
        
        
    }
    func performSegue(withIdentifier identifier: String, sender: Any?, indexPath: IndexPath) {
        
        if identifier == "enterpriseList" {
            let pass =  DetailsViewController()
            let cellData = enterprisesListAPI[indexPath.row]

            pass.details = cellData
            self.navigationController?.pushViewController(pass, animated: false)
            
        }
        
        
    }
}
extension DashViewController: DashViewModelDelegate {
    func fillingListCompanies(lista: [Enterpris]) {
        print("Na view controller nos temos elas! \n🥇\(lista[2].enterpriseName)")
        enterprisesListAPI = lista
        print(enterprisesListAPI.count)
        var types : [String] = []
        components.enterpriseTableView.reloadData()
        
        
    }
    
    
    func errorMessageDidChange(message: String) {
        print("Dash erros 😟, \(message)")
    }
    
    func startLoading() {
        print("🎆CARREGANDO..")
    }
    
    func stopLoading() {
        print("🎆Done!")
    }
    
    
}
