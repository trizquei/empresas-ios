//
//  UiColo+Ioasys.swift
//  empresas
//
//  Created by Beatriz Teles Castro on 13/01/20.
//  Copyright © 2020 Beatriz Teles Castro. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    // MARK: - background_ioasys
    /**
     Light: #EE4C77
     Dark: #EE4C77
     */
    class var background_ioasys: UIColor {
        return UIColor.init(named: "background_ioasys") ?? UIColor.white
    }
    
    // MARK: - background_bege
    /**
     Light: #EBE9D7
     Dark: #EBE9D7
     */
    class var background_bege: UIColor {
        return UIColor.init(named: "background_bege") ?? UIColor.white
    }
    
    
    
    // MARK: - label_info
    /**
     Light: #383743
     Dark: #383743
     */
    class var label_info: UIColor {
        return UIColor.init(named: "label_info") ?? UIColor.white
    }
    
    
    // MARK: - background_button_green
    /**
     Light: #57BBBC
     Dark: #57BBBC
     */
    class var background_button_green: UIColor {
        return UIColor.init(named: "background_button_green") ?? UIColor.white
    }
    
    
    // MARK: - label_green
    /**
     Light: #57BBBC
     Dark: #FFFFFF
     */
    class var label_green: UIColor {
        return UIColor.init(named: "label_green") ?? UIColor.white
    }
    
    /**
     Light: #EE4C77
     Dark: #FFFFFF
     */
    class var label_ioasys: UIColor {
        return UIColor.init(named: "label_ioasys") ?? UIColor.white
    }
}
