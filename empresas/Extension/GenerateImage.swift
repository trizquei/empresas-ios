//
//  GenerateImage.swift
//  empresas
//
//  Created by Beatriz Teles Castro on 16/01/20.
//  Copyright © 2020 Beatriz Teles Castro. All rights reserved.
//

import UIKit
class GenerateImage {

    static func getImageToShow(nameType: String) -> UIImage {
        let view : UIView = {
            let card = UIView()
            card.translatesAutoresizingMaskIntoConstraints = false
            card.backgroundColor = UIColor.clear
            card.frame = CGRect(x: 0, y: 0, width: 331, height: 194)
            card.layer.cornerRadius = 20.0
            return card
        }()
        
        let bgGradientLayer : CAGradientLayer = {
            let bg = CAGradientLayer()
            bg.backgroundColor = UIColor.clear.cgColor
            bg.colors = [ #colorLiteral(red: 0.9333333333, green: 0.2980392157, blue: 0.4666666667, alpha: 1).cgColor, #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1).cgColor]
            bg.startPoint = CGPoint.init(x: 0.5, y: 0)
            bg.endPoint = CGPoint.init(x: 0.5, y: 1)
            return bg
        }()
        
        let bgLayer : CALayer = {
            let element = CALayer()
            element.contentsGravity = CALayerContentsGravity.resizeAspectFill
            element.contents = UIImage(named: nameType)?.cgImage
            element.compositingFilter = "multiplyBlendMode"
            return element
        }()
        
        
        //screen
        bgGradientLayer.frame = (view.bounds)
        view.layer.addSublayer(bgGradientLayer)
        
        
        bgLayer.frame = (view.bounds)
        view.layer.addSublayer(bgLayer)
        
        return view.asImage()
    }

}
extension UIView {

    // Using a function since `var image` might conflict with an existing variable
    // (like on `UIImageView`)
    func asImage() -> UIImage {
        if #available(iOS 10.0, *) {
            let renderer = UIGraphicsImageRenderer(bounds: bounds)
            return renderer.image { rendererContext in
                layer.render(in: rendererContext.cgContext)
            }
        } else {
            UIGraphicsBeginImageContext(self.frame.size)
            self.layer.render(in:UIGraphicsGetCurrentContext()!)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return UIImage(cgImage: image!.cgImage!)
        }
    }
}
